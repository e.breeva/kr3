import math
import copy
import time
import random
import csv
def Mysaxpy(x,y,a):
    time_start=time.time()
    i=0
    scalar=[]
    while i<len(x):        
        scalar.append(x[i]*a+y[i])
        i+=1
    time_end=time.time()    
    return(scalar,time_end-time_start)
def Myleng(x):
    l=copy.deepcopy(x)
    for i in l:
        l[l.index(i)]=i*i
    return(math.sqrt(math.fsum(i for i in l)))
def Mygraf(x,y,a,flag):
    grafx=[]
    grafy=[]
    spisok=[]
    i=0
    while i<10000:
        if flag==1:
            x[len(x)-1]+=random.randint(-100,100)
        elif flag==0:
            x[len(x)-1]+=random.uniform(-100,100)
        z,t=Mysaxpy(x,y,a)
        grafy.append(t)
        grafx.append(Myleng(x))                
        try:
            file=open('myfile.csv','x')
            file_writer=csv.writer(file,delimiter=";",lineterminator="\r",)    
            file_writer.writerow(['x','y','z'])
        except:
            file=open("myfile.csv",'a',newline="")
        file_writer=csv.writer(file,delimiter=";",lineterminator="\r",)    
        file_writer.writerow([x,y,z])
        file.close()
        i+=1
    return(grafx,grafy)
        
