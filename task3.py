import myModule3 as mm
import matplotlib.pyplot as plt

##вектор с целыми числами
x=[3897,457356]
y=[4,5]
##вектор с вещественными числами
x1=[3897,457356]
y1=[4,5]
##скаляр
a=3
grafx,grafy=mm.Mygraf(x,y,a,1)
grafx1,grafy1=mm.Mygraf(x1,y1,a,0)
plt.xlabel('длина вектора')
plt.ylabel('время, с')
plt.grid()
plt.plot(grafx,grafy,label='int')
plt.plot(grafx1,grafy1,label='float')
plt.legend()
plt.show()
